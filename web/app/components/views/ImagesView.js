import $ from 'jquery';
var Bb = require('backbone');
var Mn = require('backbone.marionette');
import AlbumController from '../AlbumController';
var template = require('../../templates/album/images_list.jst');

export default Mn.View.extend({
    template: template,
    ui: {
        back: '#back',
        pager: '#pagination .page'
    },
    events: {
        'click @ui.back': 'backToAlbumsList',
        'click @ui.pager': 'goToPage'
    },
    backToAlbumsList: function(e){
        AlbumController.albumsPage();
    },
    goToPage: function(e){
        var albumId = $(e.currentTarget).attr("data-id");
        var page = $(e.currentTarget).attr("data-page");
        AlbumController.openAlbum(albumId, {limit: 10, page: page});
    }
});
