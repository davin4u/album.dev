import $ from 'jquery';
var Bb = require('backbone');
var Mn = require('backbone.marionette');
import AlbumController from '../AlbumController';
var template = require('../../templates/album/albums_list.jst');

export default Mn.View.extend({
    template: template,
    ui: {
        openAlbum: '.open-album'
    },
    events: {
        'click @ui.openAlbum': 'openAlbum'
    },
    openAlbum: function(e){
        var albumId = $(e.currentTarget).attr("data-id");
        AlbumController.openAlbum(albumId, {limit: 10});
    }
});
