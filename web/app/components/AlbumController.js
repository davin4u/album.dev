import $ from 'jquery';
var Bb = require('backbone');
var Mn = require('backbone.marionette');
import AlbumsView from './views/AlbumsView';
import ImagesView from './views/ImagesView';
import PaginationView from './views/PaginationView';

export default {

    albumsPage: function(){
        this.getData('/albums', {}, function(data){
            if(data != undefined){
                const Collection = Bb.Collection.extend();
                const collection = new Collection(data);

                var view = new AlbumsView({
                    collection: collection
                });
                view.render();
                $('#app').html(view.$el);
            }
        });
    },

    openAlbum: function(albumId, options){
        this.getData('/album/' + albumId, options, function(data){
            if(data != undefined){
                //items
                const Collection = Bb.Collection.extend();
                const collection = new Collection(data.items);

                var view = new ImagesView({
                    collection: collection
                });
                view.render();
                $('#app').html(view.$el);

                //pagination
                const pgCollection = Bb.Collection.extend();
                var pages = [];
                var pagesCount = Math.ceil(parseInt(data.total) / parseInt(data.limit));
                if(pagesCount > 1){
                    for(var i = 1; i <= pagesCount; i++){
                        pages[pages.length] = {aid: albumId, page: i, selected: i == data.page ? 1 : 0};
                    }
                    console.log(pages);
                    const paginatorCollection = new pgCollection(pages);
                    var pgView = new PaginationView({
                        collection: paginatorCollection
                    });
                    pgView.render();
                    $("#pagination").html(pgView.$el);
                }
            }
        });
    },

    getData: function(url, options, callback){
        options = options || {};
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: options,
            success: function(response){
                callback(response);
            }
        });
    }
};
