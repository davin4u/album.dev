import $ from 'jquery';
var Bb = require('backbone');
var Mn = require('backbone.marionette');
import AlbumController from './AlbumController';

export default Mn.Application.extend({
    region: '#app',

    initialize() {
        var $this = this;
        this.on('start', () => {
            AlbumController.albumsPage();
        });
    }
});
