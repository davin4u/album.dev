<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Album;
use AppBundle\Entity\Image;

class LoadTestData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //create 5 albums
        for($i = 1; $i <= 5; $i++){
            $model = new Album();
            $model->setName('Album ' . $i);
            $manager->persist($model);
            $manager->flush();

            //create images for album
            $imagesCount = $i == 1 ? 5 : $i * 10;
            for($j = 1; $j <= $imagesCount; $j++){
                $imgModel = new Image();
                $imgModel->setName('Image ' . $j);
                $imgModel->setAlbumId($model->getId());
                $imgModel->setPath('/upload/img' . rand(1,4) . '.png');
                $manager->persist($imgModel);
                $manager->flush();
            }
        }
    }
}
