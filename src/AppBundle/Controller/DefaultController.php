<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Helpers\SerializedResponse;

class DefaultController extends Controller
{

    private $response;

    public function __construct()
    {
        $this->response = new SerializedResponse();
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }

    /**
    * @Route("/albums", name="albums")
    */
    public function albumsAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Album');
        $items = $repository->findAll();
        return $this->response->get($items, 'json');
    }

    /**
    * @Route("/album/{id}", name="album")
    */
    public function albumAction($id, Request $request)
    {
        $limit = $request->get('limit', 10);
        $page = $request->get('page', 1);

        $repository = $this->getDoctrine()->getRepository('AppBundle:Image');
        $query = $repository->createQueryBuilder('p')->where('p.albumId = :id')->setParameter('id', $id);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', $page),
            $limit
        );

        $response = array('items' => $pagination, 'total' => $pagination->getTotalItemCount(), 'limit' => $limit, 'page' => $page);
        return $this->response->get($response, 'json');
    }
}
