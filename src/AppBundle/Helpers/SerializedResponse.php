<?php

namespace AppBundle\Helpers;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;

class SerializedResponse
{
    private $encoders = array();
    private $normalizers = array();
    private $serializer;

    public function __construct($encoders = array(), $normalizers = array())
    {
        $this->encoders = $encoders;
        $this->normalizers = $normalizers;
        if(empty($this->encoders)){
            $this->encoders = array(new JsonEncoder());
        }
        if(empty($this->normalizers)){
            $this->normalizers = array(new ObjectNormalizer());
        }
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
    }

    /**
    * Set encoders objects array
    * @param array $encoders
    */
    public function setEncoders($encoders = array())
    {
        if(!empty($encoders)){
            $this->encoders = $encoders;
        }
    }

    /**
    * Set normilizers objects array
    * @param array $normilizers
    */
    public function setNormilizers($normilizers = array())
    {
        if(!empty($normilizers)){
            $this->normilizers = $normilizers;
        }
    }

    /**
    * Update an instance of serializer
    *
    */
    public function update()
    {
        if(!empty($this->encoders) && !empty($this->normilizers)){
            $this->serializer = new Serializer($this->normalizers, $this->encoders);
        }
    }

    /**
    * Build serizlized response
    * @param collection $data
    * @param string $type
    * @return Symfony\Component\HttpFoundation\Response
    */
    public function get($data, $type = 'json')
    {
        $response = new Response('Content', Response::HTTP_OK);
        return $response->setContent($this->serializer->serialize($data, $type));
    }
}
